var gulp   = require('gulp'),
    harp   = require('harp');


gulp.task('serve', function() {
  harp.server(__dirname + '/style-guide', {
    port: 9000
  })
});

gulp.task('build', function(done) {
  process.env.NODE_ENV = 'production';

  harp.compile(__dirname + '/style-guide', __dirname + '/www', function(errors) {
    if(errors) {
      console.log(JSON.stringify(errors, null, 2));
      process.exit(1);
    }

    return process.exit(0);
  });
});

// Alias `gulp build` as `gulp compile`
gulp.task('compile', ['build']);

gulp.task('default', ['serve']);

