# Fractal

An infinite series of repeating patterns.

## Style guide

To serve the style guide locally:

```
$ gulp serve
```

To compile the style guide to static HTML:

```
$ gulp compile
```


## Sass library

To include the Sass library in your project:

```sass
// Override Fractal variables _before_ you import the library.

// Include Normalize
$include-normalize: true

@import 'your_projects/bower_components/fractal/sass/index'
```

